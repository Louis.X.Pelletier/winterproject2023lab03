import java.util.Scanner;
public class ApplianceStore{
	
	public static void main(String[] args){
		
		Dishwasher[] dishwasher = new Dishwasher[4];
		
		for (int i = 0; i < dishwasher.length; i++){
			Scanner reader = new Scanner(System.in);
			Dishwasher temp = new Dishwasher();
			
			System.out.println("Enter the Dishwasher's name:");
			temp.name = reader.nextLine();
			
			System.out.println("Enter the Dishwasher's volume(int):");
			temp.volume = Integer.parseInt(reader.nextLine());

			System.out.println("Enter the Dishwasher's water temperature(int):");
			temp.waterTemperature = Integer.parseInt(reader.nextLine());
			
			System.out.println("Precise if the dishwasher has pods in it(true or false):");
			temp.pods = reader.nextBoolean();
			
			dishwasher[i]=temp;
		}
		
		System.out.println(dishwasher[3].name);
		System.out.println(dishwasher[3].volume);
		System.out.println(dishwasher[3].waterTemperature);
		System.out.println(dishwasher[3].pods);
		
		Dishwasher dw = new Dishwasher();
		
		dw.storeDishes(dishwasher[0].volume);
		dw.cleanDishes(dishwasher[0].waterTemperature, dishwasher[1].pods);
	}
	
}