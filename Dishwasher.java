import java.util.Scanner;
public class Dishwasher{
	
	public String name;
	public int volume;
	public int waterTemperature;
	public boolean pods;
	
	public void storeDishes(int a){
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Insert the volume of your dishes:");
		int dishVolume = reader.nextInt();
		System.out.println(dishVolume);
		
		int maxStorage = a/dishVolume;
		System.out.println("Your dishwasher can store "+ maxStorage + " dishes");
		
	}
	
	public void cleanDishes(int a, boolean b){
		
		if (a >= 30 && b == true){
			System.out.println("You can use your dishwasher to clean your dishes");
		}
		else{
			System.out.println("You cannot use your dishwasher to clean your dishes");
		}
	}
}